<?php

return array(

	'image_management'    => 'Image Management',
	'image_update'        => 'Update an Image',
	'image_delete'        => 'Delete an Image',
	'create_a_new_image'  => 'Create a New Image',

);