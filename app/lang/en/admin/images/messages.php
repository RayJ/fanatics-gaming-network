<?php

return array(

	'does_not_exist' => 'Image does not exist.',

	'create' => array(
		'error'   => 'Image was not created, please try again.',
		'success' => 'Image created successfully.'
	),

	'update' => array(
		'error'   => 'Image was not updated, please try again',
		'success' => 'Image updated successfully.'
	),

	'delete' => array(
		'error'   => 'There was an issue deleting the image. Please try again.',
		'success' => 'The image was deleted successfully.'
	)

);
