<?php

class Image extends Eloquent {
	/**
	 * Get the post's category.
	 *
	 * @return Category
	 */
	public function category()
	{
		return $this->belongsTo('Category', 'category_id');
	}
}