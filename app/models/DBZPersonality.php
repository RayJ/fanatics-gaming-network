<?php

class DBZPersonality extends Eloquent {
	protected $table = "dbzpersonalities";
	protected $guarded = [];
	
	public function cards()
	{
		return $this->hasMany('DBZCard');
	}
	
	public function alignment()
	{
		return $this->belongsTo('DBZAlignment');
	}

	public function personality_type()
	{
		return $this->belongsTo('DBZPersonalityType', 'personality_type_id');
	}
}