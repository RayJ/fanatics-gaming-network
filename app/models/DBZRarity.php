<?php

class DBZRarity extends Eloquent {
	protected $table = "dbzrarity";
	// protected $guarded = [];
	
	public function cards()
	{
		return $this->hasMany('DBZCard');
	}
}