<?php

class DBZSet extends Eloquent {
	protected $table = "dbzsets";
	// protected $guarded = [];
	
	public function cards()
	{
		return $this->hasMany('DBZCard');
	}
}