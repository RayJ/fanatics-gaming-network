<?php

class DBZCard extends Eloquent {
	protected $table = "dbzcards";
	// protected $fillable = array('title', 'content', 'set_id', 'style_id', 'type_id', 'personality_id', 'number', 'rarity_id', 'alignment_id', 'level_id');
	// protected $guarded = [];
	
	public function set()
	{
		return $this->belongsTo('DBZSet');
	}

	public function style()
	{
		return $this->belongsTo('DBZStyle');
	}
	
	public function type()
	{
		return $this->belongsTo('DBZType');
	}
	
	public function personality()
	{
		return $this->belongsTo('DBZPersonality');
	}
	
	public function rarity()
	{
		return $this->belongsTo('DBZRarity');
	}
	
	public function alignment()
	{
		return $this->belongsTo('DBZAlignment');
	}
	
	public function level()
	{
		return $this->belongsTo('DBZLevel');
	}

	public function decks()
	{
		return $this->belongsToMany('DBZDeck', 'dbzcards_dbzdecks');
	}
	
}