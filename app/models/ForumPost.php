<?php

class ForumPost extends Eloquent {

	protected $table = 'GDN_Discussion';

	public function comments()
	{
		return $this->hasMany('ForumComment', 'DiscussionID', 'DiscussionID');
	}

	public function user()
	{
		return $this->hasMany('ForumUser', 'InsertUserID', 'UserID');
	}
}