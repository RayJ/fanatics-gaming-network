<?php

class DBZType extends Eloquent {
	protected $table = "dbztypes";
	// protected $guarded = [];

	public function cards()
	{
		return $this->hasMany('DBZCard');
	}
}