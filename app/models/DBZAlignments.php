<?php

class DBZAlignment extends Eloquent {
	protected $table = "dbzalignments";

	public function cards()
	{
		return $this->hasMany('DBZCard');
	}

	public function personalities()
	{
		return $this->hasMany('DBZPersonality');
	}
}