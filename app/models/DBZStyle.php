<?php

class DBZStyle extends Eloquent {
	protected $table = "dbzstyles";
	// protected $guarded = [];
	
	public function cards()
	{
		return $this->hasMany('DBZCard');
	}
}