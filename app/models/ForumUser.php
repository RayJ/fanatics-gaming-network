<?php

class ForumUser extends Eloquent {

	protected $table = 'GDN_User';

	public function comments()
	{
		return $this->hasMany('ForumComment', 'InsertUserID', 'UserID');
	}

	public function posts()
	{
		return $this->hasMany('ForumPost', 'InsertUserID', 'UserID');
	}
}