<?php

class DBZLevel extends Eloquent {
	protected $table = "dbzlevels";
	// protected $guarded = [];

	public function cards()
	{
		return $this->hasMany('DBZCard');
	}
}