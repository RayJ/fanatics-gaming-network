<?php

class DBZPersonalityType extends Eloquent {
	protected $table = "dbzpersonalitytypes";
	// protected $guarded = [];

	public function personalities()
	{
		return $this->hasMany('DBZPersonality');
	}
}