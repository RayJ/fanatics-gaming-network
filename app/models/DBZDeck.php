<?php

class DBZDeck extends Eloquent {
	protected $table = "dbzdecks";

	public function style()
	{
		return $this->belongsTo('DBZStyle');
	}
	
	public function personality()
	{
		return $this->belongsTo('DBZPersonality');
	}
	
	public function cards()
	{
		return $this->belongsToMany('DBZCard', 'dbzcards_dbzdecks');
	}
}