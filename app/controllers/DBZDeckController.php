<?php

class DBZDeckController extends BaseController {

    /**
     * DBZCard Model
     * @var Post
     */
    protected $deck;
    protected $user;

    /**
     * Inject the models.
     * @param DBZCard $deck
     */
    public function __construct(DBZDeck $deck, User $user)
    {
        parent::__construct();

        $this->deck = $deck;
        $this->user = $user;
    }
    
	/**
	 * Returns all the decks.
	 *
	 * @return View
	 */
	public function getIndex()
	{
		// if(Session::has('dbz-filter'))
		// {
		// 	$filter = Session::get('dbz-filter');
		// 	$style = ($filter['style'] == 'all' ? null : $filter['style']);
		// 	$personality = ($filter['personality'] == 'all' ? null : $filter['personality']); 
		// 	$set = ($filter['set'] == 'all' ? null : $filter['set']);
		// 	$type = (count($filter['type']) ? $filter['type'] : null);

		// 	$cards = DBZCard::query(); //use query object so we can modify it conditionally.
			
		// 	if($style) $cards=$cards->where('style_id', $style);
		// 	if($personality) $cards=$cards->where('personality_id', $personality);
		// 	if($set) $cards=$cards->where('set_id', $set);
		// 	if($type) $cards=$cards->whereIn('type_id', $type);

		// 	// $cards = $cards->get();
		// 	$cards = $cards->distinct()->paginate(15);
		// 	// var_dump($cards);
		// } else {
		// 	Session::put('dbz-filter', [
		// 		'state' => 'closed',
		// 		'style' => null,
		// 		'personality' => null,
		// 		'set' => null,
		// 		'type' => null,
		// 		'amount' => 15,
		// 	]);
		// 	$cards = DBZCard::paginate(15);
			
		// }
		// $type = (Session::get('dbz-filter')['type'] ? Session::get('dbz-filter')['type'] : [] ); 
		// return View::make('site/dbzcards/index', compact('cards', 'type'));
	}

	/**
	 * Form for creating a deck
	 *
	 * @return View
	 */
	public function getCreate()
	{
		$personalities = DBZPersonality::where('personality_type_id', '=', 1)->get()->lists('title', 'id');
        $styles = DBZStyle::lists('title', 'id');
        
        list($user,$redirect) = $this->user->checkAuthAndRedirect('user');
        if($redirect){return $redirect;}

        // Show the page
		return View::make('site/dbzdecks/create', compact('personalities', 'styles', 'user'));
	}

	public function postCreate()
	{
		
		$deck = new DBZDeck;

		$deck->personality_id = Input::get('personality');
		$deck->style_id = Input::get('style');
		$deck->user_id = Auth::id();
		$deck->title = Input::get('title');
		$deck->description = Input::get('description');
		$deck->public = Input::get('public') ? 1 : 0;
		$deck->save();

		return Redirect::to('decks/dbz/edit/'.$deck->id);
		// $data = [$personality, $style, $title, $description, $public];
		// var_dump($deck);
	}

	public function getEdit($dbzdeck)
	{
		$deck = DBZDeck::find($dbzdeck);
		$mastery = DBZCard::where('style_id', $deck->style_id)->where('type_id',9)->get()->lists('title', 'id');
		$level[1] = DBZCard::where('type_id',8)->where('personality_id',$deck->personality_id)->where('level_id', 1)->get()->lists('title', 'id');
		$level[2] = DBZCard::where('type_id',8)->where('personality_id',$deck->personality_id)->where('level_id', 2)->get()->lists('title', 'id');
		$level[3] = DBZCard::where('type_id',8)->where('personality_id',$deck->personality_id)->where('level_id', 3)->get()->lists('title', 'id');
		$level[4] = DBZCard::where('type_id',8)->where('personality_id',$deck->personality_id)->where('level_id', 4)->get()->lists('title', 'id');
		// var_dump($mastery);
		return View::make('site/dbzdecks/edit', compact('deck', 'mastery', 'level'));
	}
	 /**
	 * Sets filters in session and returns all the decks that match the filters.
	 *
	 * @return View
	 */
	public function postIndex()
	{
		//ALERT!!!
		//The inital pagination after post always returns 15 results on the first page regardless of the number set in the pagination
		// Session::put('dbz-filter', [
		// 	'state' => 'open',
		// 	'style' => Input::get('style'),
		// 	'personality' => Input::get('personality'),
		// 	'set' => Input::get('set'),
		// 	'type' => Input::get('type'),
		// 	'amount' => 15,
		// 	// 'style' => Input::get('type'),
		// ]);
		// if(Session::has('dbz-filter'))
		// {
		// 	$filter = Session::get('dbz-filter');
		// 	$style = ($filter['style'] == 'all' ? null : $filter['style']);
		// 	$personality = ($filter['personality'] == 'all' ? null : $filter['personality']); 
		// 	$set = ($filter['set'] == 'all' ? null : $filter['set']);
		// 	$type = (count($filter['type']) ? $filter['type'] : null);

		// 	$cards = DBZCard::query(); //use query object so we can modify it conditionally
			
		// 	if($style) $cards=$cards->where('style_id', $style);
		// 	if($personality) $cards=$cards->where('personality_id', $personality);
		// 	if($set) $cards=$cards->where('set_id', $set);
		// 	if($type) $cards=$cards->whereIn('type_id', $type);

		// 	$cards = $cards->paginate(Session::get('amount'));
		// 	// var_dump($cards);
		// } else {
		// 	$cards = DBZCard::paginate(15);
			
		// }
		// $type = (Session::get('dbz-filter')['type'] ? Session::get('dbz-filter')['type'] : [] ); 
		
		// // Show the page
		// return View::make('site/dbzcards/index', compact('cards', 'type'));
	}

	/**
	 * View a deck.
	 *
	 * @param  id  $deck_id
	 * @return View
	 * @throws NotFoundHttpException
	 */
	public function getDeck($deck_id)
	{
		// Get this deck data
		$deck = $this->deck->find($deck_id);

		// Check if the deck exists
		if (is_null($deck))
		{
			// If we ended up in here, it means that
			// a page or a blog post didn't exist.
			// So, this means that it is time for
			// 404 error page.
			return App::abort(404);
		}

		// Show the page
		return View::make('site/dbzdecks/view_deck', compact('deck'));
	}

}
