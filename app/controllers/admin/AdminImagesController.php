<?php

class AdminImagesController extends AdminController {


    /**
     * Image Model
     * @var Image
     */
    protected $image;

    /**
     * Inject the models.
     * @param Image $image
     */
    public function __construct(Image $image)
    {
        parent::__construct();
        $this->image = $image;
    }

    /**
     * Show a list of all the image images.
     *
     * @return View
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/images/title.image_management');

        // Grab all the images
        $images = $this->image;

        // var_dump($images);
        // Show the page
        return View::make('admin/images/index', compact('images', 'title'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
        // Title
        $title = Lang::get('admin/images/title.create_a_new_image');

        // Show the page
        return View::make('admin/images/create_edit', compact('title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
        // Declare the rules for the form validation
        //ToDo: add category/file validations
        $rules = array(
            'title'   => 'required|min:3',
            'filename' => 'required|min:3'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the image image data
            $this->image->title            = Input::get('title');
            $this->image->category_id      = Input::get('category_id');
            $this->image->filename         = Input::get('filename');

            // Upload the image
            $file = Input::file('file');
            $destination = "assets/img/" . Category::find($this->image->category_id)->slug;
            $upload_success = $file->move($destination, $this->image->filename);

            // Was the image image created?
            if($this->image->save())
            {
                // Redirect to the new image image page
                return Redirect::to('admin/images/' . $this->image->id . '/edit')->with('success', Lang::get('admin/images/messages.create.success'));
            }

            // Redirect to the image image create page
            return Redirect::to('admin/images/create')->with('error', Lang::get('admin/images/messages.create.error'));
        }

        // Form validation failed
        return Redirect::to('admin/images/create')->withInput()->withErrors($validator);
	}

    /**
     * Display the specified resource.
     *
     * @param $image
     * @return Response
     */
	public function getShow($image)
	{
        // redirect to the frontend
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param $image
     * @return Response
     */
	public function getEdit($image)
	{
        // Title
        $title = Lang::get('admin/images/title.image_update');

        // Show the page
        return View::make('admin/images/create_edit', compact('image', 'title'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $image
     * @return Response
     */
	public function postEdit($image)
	{

        // Declare the rules for the form validation
        $rules = array(
            'title'   => 'required|min:3',
            'content' => 'required|min:3'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the image image data
            $image->title            = Input::get('title');
            $image->slug             = Str::slug(Input::get('title'));
            $image->content          = Input::get('content');
            $image->meta_title       = Input::get('meta-title');
            $image->meta_description = Input::get('meta-description');
            $image->meta_keywords    = Input::get('meta-keywords');

            // Was the image image updated?
            if($image->save())
            {
                // Redirect to the new image image page
                return Redirect::to('admin/images/' . $image->id . '/edit')->with('success', Lang::get('admin/images/messages.update.success'));
            }

            // Redirect to the images image management page
            return Redirect::to('admin/images/' . $image->id . '/edit')->with('error', Lang::get('admin/images/messages.update.error'));
        }

        // Form validation failed
        return Redirect::to('admin/images/' . $image->id . '/edit')->withInput()->withErrors($validator);
	}


    /**
     * Remove the specified resource from storage.
     *
     * @param $image
     * @return Response
     */
    public function getDelete($image)
    {
        // Title
        $title = Lang::get('admin/images/title.image_delete');

        // Show the page
        return View::make('admin/images/delete', compact('image', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $image
     * @return Response
     */
    public function postDelete($image)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        //ToDo: delete file from database
        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $image->id;
            $image->delete();

            // Was the image image deleted?
            $image = image::find($id);
            if(empty($image))
            {
                // Redirect to the image images management page
                return Redirect::to('admin/images')->with('success', Lang::get('admin/images/messages.delete.success'));
            }
        }
        // There was a problem deleting the image image
        return Redirect::to('admin/images')->with('error', Lang::get('admin/images/messages.delete.error'));
    }

    /**
     * Show a list of all the image images formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $images = image::select(array('images.id', 'images.title', 'images.category_id as category', 'images.filename', 'images.created_at'));

        return Datatables::of($images)

        // ->edit_column('category', '{{ DB::table(\'categories\')->where(\'id\', \'=\', $category) }}')

        // ->add_column('actions', '<a href="{{{ URL::to(\'admin/images/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-xs iframe" >{{{ Lang::get(\'button.edit\') }}}</a>
        //         <a href="{{{ URL::to(\'admin/images/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>
            // ')

        ->remove_column('id')

        ->make();
    }

}