<?php

class DBZCardController extends BaseController {

    /**
     * DBZCard Model
     * @var Post
     */
    protected $card;

    /**
     * Inject the models.
     * @param DBZCard $card
     */
    public function __construct(DBZCard $card)
    {
        parent::__construct();

        $this->card = $card;
    }
    
	/**
	 * Returns all the cards.
	 *
	 * @return View
	 */
	public function getIndex()
	{
		//$style = 'all', $personality = 'all', $set = 'all', $type = null, $order = 'desc', $page = 1
		// Get all the blog posts
		// Session::forget('dbz-filter');
		if(Session::has('dbz-filter'))
		{
			$filter = Session::get('dbz-filter');
			$style = ($filter['style'] == 'all' ? null : $filter['style']);
			$personality = ($filter['personality'] == 'all' ? null : $filter['personality']); 
			$set = ($filter['set'] == 'all' ? null : $filter['set']);
			$type = (count($filter['type']) ? $filter['type'] : null);

			$cards = DBZCard::query(); //use query object so we can modify it conditionally.
			
			if($style) $cards=$cards->where('style_id', $style);
			if($personality) $cards=$cards->where('personality_id', $personality);
			if($set) $cards=$cards->where('set_id', $set);
			if($type) $cards=$cards->whereIn('type_id', $type);

			// $cards = $cards->get();
			$cards = $cards->distinct()->paginate(15);
			// var_dump($cards);
		} else {
			Session::put('dbz-filter', [
				'state' => 'closed',
				'style' => null,
				'personality' => null,
				'set' => null,
				'type' => null,
				'amount' => 15,
			]);
			$cards = DBZCard::paginate(15);
			
		}
		$type = (Session::get('dbz-filter')['type'] ? Session::get('dbz-filter')['type'] : [] ); 
		return View::make('site/dbzcards/index', compact('cards', 'type'));		

		// Show the page
		// return View::make('site/dbzcards/index', compact('cards'));
	}

	/**
	 * Sets filters in session and returns all the cards that amtch the filters.
	 *
	 * @return View
	 */
	public function postIndex()
	{
		//ALERT!!!
		//The inital pagination after post always returns 15 results on the first page regardless of the number set in the pagination
		Session::put('dbz-filter', [
			'state' => 'open',
			'style' => Input::get('style'),
			'personality' => Input::get('personality'),
			'set' => Input::get('set'),
			'type' => Input::get('type'),
			'amount' => 15,
			// 'style' => Input::get('type'),
		]);
		if(Session::has('dbz-filter'))
		{
			$filter = Session::get('dbz-filter');
			$style = ($filter['style'] == 'all' ? null : $filter['style']);
			$personality = ($filter['personality'] == 'all' ? null : $filter['personality']); 
			$set = ($filter['set'] == 'all' ? null : $filter['set']);
			$type = (count($filter['type']) ? $filter['type'] : null);

			$cards = DBZCard::query(); //use query object so we can modify it conditionally
			
			if($style) $cards=$cards->where('style_id', $style);
			if($personality) $cards=$cards->where('personality_id', $personality);
			if($set) $cards=$cards->where('set_id', $set);
			if($type) $cards=$cards->whereIn('type_id', $type);

			$cards = $cards->paginate(Session::get('amount'));
			// var_dump($cards);
		} else {
			$cards = DBZCard::paginate(15);
			
		}
		$type = (Session::get('dbz-filter')['type'] ? Session::get('dbz-filter')['type'] : [] ); 
		
		// Show the page
		return View::make('site/dbzcards/index', compact('cards', 'type'));
	}

	/**
	 * Returns all the cards.
	 *
	 * @param  id  $set
	 * @return View
	 * @throws NotFoundHttpException
	 */
	public function getSet($set_id)
	{
		// Get all the cards in the set
		$cards = $this->card->where('set_id', $set_id)->get();
		$set = DBZSet::find($set_id)->first();
		// Show the page
		return View::make('site/dbzcards/set', compact('cards','set'));
	}
	/**
	 * View a card.
	 *
	 * @param  id  $card_id
	 * @return View
	 * @throws NotFoundHttpException
	 */
	public function getCard($card_id)
	{
		// Get this card data
		$card = $this->card->find($card_id);

		// Check if the card exists
		if (is_null($card))
		{
			// If we ended up in here, it means that
			// a page or a blog post didn't exist.
			// So, this means that it is time for
			// 404 error page.
			return App::abort(404);
		}

		// Show the page
		return View::make('site/dbzcards/view_card', compact('card'));
	}

}
