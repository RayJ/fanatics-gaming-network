<?php

class DBZFiltersComposer {
	/**
	 * body class will go three methods deep on the url
	 * @author Raymond Linton
	 * @param  object $view 
	 * @return void       
	 */
    public function compose($view)
    {    
        // $personalities = DB::table('dbzpersonalities')->select('id', 'title')->where('personality_type_id', 1)->get();
        $personalities = DBZPersonality::where('personality_type_id', '=', 1)->get()->lists('title', 'id');
        $styles = DBZStyle::lists('title', 'id');
        $sets = DBZSet::lists('title', 'id');
        $view->with(array(
        	'personalities' => $personalities,
        	'styles' => $styles,
            'sets' => $sets,
    	));
    }
}