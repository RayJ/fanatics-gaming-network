<?php

class RecentCommentsComposer {
	/**
	 * body class will go three methods deep on the url
	 * @author Raymond Linton
	 * @param  object $view 
	 * @return void       
	 */
    public function compose($view)
    {    
        $comments = Comment::orderBy('created_at', 'desc')->take(10)->get();
        $view->with('comments', $comments);
    }
}