<?php

class BodyClassComposer {
	/**
	 * body class will go three methods deep on the url
	 * @author Raymond Linton
	 * @param  object $view 
	 * @return void       
	 */
    public function compose($view)
    {    
        $bodyClass = (is_null(Request::segment(1)) || is_numeric(Request::segment(1))) ? 'home ' : Request::segment(1) . ' ';
        $bodyClass .= (is_null(Request::segment(2)) || is_numeric(Request::segment(2))) ? 'index ' : Request::segment(2) . ' ';
        $bodyClass .= (is_null(Request::segment(3)) || is_numeric(Request::segment(3))) ? 'index ' : Request::segment(3) . ' ';
        $view->with('bodyClass', $bodyClass);
    }
}