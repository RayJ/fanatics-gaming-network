@extends('site.layouts.fgn')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.profile') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="userForm">
    <div class="crossbar"></div>
    <div class="crossTitle"><h4>User Profile</h4></div>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Username</th>
            <th>Signed Up</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{{$user->id}}}</td>
            <td>{{{$user->username}}}</td>
            <td>{{{$user->joined()}}}</td>
        </tr>
        </tbody>
    </table>
</div>
@stop
