@extends('site.layouts.fgn')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.login') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="userForm">
    <div class="crossbar"></div>
    <div class="crossTitle"><h4>User Login</h4></div>
    {{ Confide::makeLoginForm()->render() }}
</div>
@stop
