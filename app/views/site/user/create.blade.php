@extends('site.layouts.fgn')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.register') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="userForm">
	<div class="crossbar"></div>
    <div class="crossTitle"><h4>User Registration</h4></div>
	{{ Confide::makeSignupForm()->render() }}
</div>
@stop
