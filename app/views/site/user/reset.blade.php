@extends('site.layouts.fgn')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.forgot_password') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="userForm">
	<div class="crossbar"></div>
    <div class="crossTitle"><h4>Forgot Password</h4></div>
	{{ Confide::makeResetPasswordForm($token)->render() }}
</div>
@stop
