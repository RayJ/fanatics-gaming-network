@extends('site.layouts.master')

@section('app_css')
	<link rel="stylesheet" href="{{ asset('assets/css/fgn.css') }}">
@stop

@section('site_layout')
	<div class="page">
		<header>
			@include('site.layouts.partials.header')
		</header>
		<div class="row">
			<div class="small-12 medium-8 columns">
				<div class="content">
					@yield('content')
				</div>
			</div>
			<div class="medium-4 columns">
				<div class="rightPanel">
					@section('sidebar')
						@include('site.sidebars.community')
					@show
				</div>
			</div>
		</div>
	</div>
	<footer>
		@include('site.layouts.partials.footer')
	</footer>
@stop