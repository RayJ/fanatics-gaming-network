<div class="row">
	<p class="legal">This is a fan site for many popular CCGs, LCGs, and Miniature Games. Many images and trademarks are used without permission.</p>
	<p class="legal">&copy; 2000-2014 Fanatics Gaming Network/DBZ Fanatics.</p>
	<p class="legal">All other trademarks and copyrights are the property of their respective owners.</p>
</div>