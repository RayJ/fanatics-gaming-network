<div class="crossbar hide-for-small"></div>
<div class="row navigation">
	<div class="small-12 columns contain-to-grid">
		<img class="logo hide-for-small" src="{{ asset('assets/img/logos/FGN-Logo.png') }}" alt="">
		<nav class="top-bar" data-topbar role="navigation">
			<ul class="title-area">
				<li class="name show-for-small">
					<h1>Fanatics Gaming Network</h1>
				</li>
				<li class="toggle-topbar menu-icon">
					<a href="#"><span>Menu</span></a>
				</li>
			</ul>
			<section class="top-bar-section">
				<ul class="right">
					@if(Auth::check())
						@if(Auth::user()->hasRole('admin'))
						<li><a href="/admin">Admin</a></li>
						@endif
						<li><a href="/user">Account</a></li>
						<li><a href="/user/logout">Logout</a></li>
					@else
						<li><a href="/user/login">Login</a></li>
						<li><a href="/user/create">Register</a></li>
					@endif
				</ul>
				<ul class="left">
					<li><a href="/">Home</a></li>
					<li><a href="/cards/dbz">Card Database</a></li>
					<!-- <li><a href="/decks/dbz/create">Deck Builder</a></li> -->
					<li><a href="http://www.z-warriors.com">Forums</a></li>
				</ul>
			</section>
		</nav>
	</div>
</div>