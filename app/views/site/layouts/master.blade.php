<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<!-- Basic Page Needs
		================================================== -->
		<meta charset="utf-8" />
		<title>
			@section('title')
			Fanatics Gaming Network
			@show
		</title>
		<meta name="google-site-verification" content="YRwNVfEwSHWdxXrfBj4t2JgmdeS19jgbSvkANkYqz8Y" />
		<meta name="_token" content="{{ csrf_token() }}" />
		<meta name="keywords" content="@yield('meta_keywords')" />
		<meta name="author" content="Raymond Linton" />
		<meta name="description" content="@yield('meta_description')" />

		<!-- Mobile Specific Metas
		================================================== -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes" />
		
		<!-- CSS
		================================================== -->
		<link rel="stylesheet" href="{{ asset('bower_components/slick-carousel/slick/slick.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/fgn.css') }}">
		@yield('app_css')
		<!-- Javascripts
		================================================== -->
        <script src="{{ asset('bower_components/modernizr/modernizr.js') }}"></script>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!-- Favicons
		================================================== -->
		<!-- <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}">
		 --><link rel="shortcut icon" href="{{{ asset('assets/img/favicon.ico') }}}">
	</head>
	<body class="@yield('bodyClass', $bodyClass)">
		@yield('site_layout')

        <script src="{{ asset('bower_components/jquery/dist/jquery.js') }}"></script>
        <script src="{{ asset('bower_components/foundation/js/foundation.min.js') }}"></script>
        <script src="{{ asset('bower_components/slick-carousel/slick/slick.min.js') }}"></script>
        <script src="{{ asset('assets/js/app.js') }}"></script>
        @yield('app_js')
	</body>
</html>
