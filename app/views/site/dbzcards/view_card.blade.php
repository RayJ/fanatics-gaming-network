@extends('site.layouts.fgn')

{{-- Content --}}
@section('content')

<div class="panel">
	<h4>{{ $card->title }}</h4>
	<div class="cardinfo">
		<span class="label content">Content:</span>
		<span class="info content">{{$card->content}}</span>

		<span class="label">Rarity:</span>
		<span class="info">{{$card->rarity->title}}</span>
		<span class="label">Number:</span>
		<span class="info">{{$card->rarity->slug}}{{$card->number}}</span>
		<span class="label">Type:</span>
		<span class="info">{{$card->type->title}}</span>
	</div>
	<img class="cardImage" alt="{{ $card->title }}" src="{{ asset('assets/img/dbz/cards/set-' . $card->set->id .'/'. strtolower($card->rarity->slug) . $card->number . '.jpg') }}" />
</div>

@stop