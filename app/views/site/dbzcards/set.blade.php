@extends('site.layouts.fgn')

{{-- Content --}}
@section('content')

<div class="panel">
	<h2>{{ $set->title }}</h2>
	<ul>
		@foreach($cards as $card)
		<li><a href="/cards/dbz/card/{{$card->id}}">{{$card->rarity->slug}}{{$card->number}} {{$card->title}}</a></li>
		@endforeach
	</ul>
</div>
@stop