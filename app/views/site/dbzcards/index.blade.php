@extends('site.layouts.fgn')

{{-- Content --}}
@section('content')

<div class="mainPanel">
	<div class="crossbar"></div>
    <div class="crossTitle"><h1>DBZ Card Database</h1></div>
	<div class="games">
		@include('site.dbzcards.filters')
		{{ $cards->links() }}
		<ul class="cardlist">
		@foreach($cards as $card)
			<li class="row">
				<div class="small-3 medium-2 columns">
					<img data-reveal-id="{{ $card->rarity->slug }}{{ $card->number }}" class="cardImage" alt="{{ $card->title }}" src="{{ asset('assets/img/dbz/cards/set-' . $card->set->id .'/'. strtolower($card->rarity->slug) . $card->number . '.jpg') }}" />
				</div>
				<div class="small-7 medium-9 columns">
					<h3>{{ $card->title }}</h3>
					<h4>
						{{ $card->type->title }}
						@if($card->personality)
						- {{ $card->personality->title }}
						@elseif($card->style)
						- {{ $card->style->title }} Style
						@endif
						@if($card->level)
						- {{ $card->level->title }}
						@endif
						@if($card->alignment)
						- {{ $card->alignment->title }}
						@endif
					</h4>
					<p>{{ $card->content }}</p>
				</div>
				<div class="small-2 medium-1 columns">
					<h6>{{ $card->set->title }}</h6>
					<h6>{{ $card->rarity->slug }}{{ $card->number }}</h6>
				</div>
			</li>
		@endforeach
		</ul>
		{{ $cards->links() }}
	</div>
</div>

@foreach($cards as $card)
<div id="{{ $card->rarity->slug }}{{ $card->number }}" class="reveal-modal card @if($card->type->title == 'Mastery') mastery @endif" data-reveal>
	<img class="cardImage" alt="{{ $card->title }}" src="{{ asset('assets/img/dbz/cards/set-' . $card->set->id .'/'. strtolower($card->rarity->slug) . $card->number . '.jpg') }}" />
</div>
@endforeach
@stop

@section('app_js')
<script>
function filter() {
	console.log('Applying Filters');
	$('#dbz-filter').submit();
	// $post('/cards/dbz', $('#dbz-filter').serialize());
};
$(document).ready(function() {
	$('.filter').change(function() {
		filter();
	});
});
</script>
@stop