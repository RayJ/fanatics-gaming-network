<div class="filters">
	{{ Form::open(array('id' => 'dbz-filter')) }}
	<div class="cardType">
		{{ Form::checkbox('type[]','1', in_array(1, $type), ['id' => 'filter_ally', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_ally', 'Ally') }}
		{{ Form::checkbox('type[]','2', in_array(2, $type), ['id' => 'filter_dragonball', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_dragonball', 'Dragon Ball') }}
		{{ Form::checkbox('type[]','3', in_array(3, $type), ['id' => 'filter_drill', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_drill', 'Drill') }}
		{{ Form::checkbox('type[]','4', in_array(4, $type), ['id' => 'filter_setup', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_setup', 'Setup') }}
		{{ Form::checkbox('type[]','5', in_array(5, $type), ['id' => 'filter_event', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_event', 'Event') }}
		{{ Form::checkbox('type[]','6', in_array(6, $type), ['id' => 'filter_energy', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_energy', 'Energy Combat') }}
		{{ Form::checkbox('type[]','7', in_array(7, $type), ['id' => 'filter_physical', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_physical', 'Physical Combat') }}
		{{ Form::checkbox('type[]','8', in_array(8, $type), ['id' => 'filter_personality', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_personality', 'Main Personality') }}
		{{ Form::checkbox('type[]','9', in_array(9, $type), ['id' => 'filter_mastery', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_mastery', 'Mastery') }}
	</div>
	<div class="set">
		{{ form::select('set', ['all' => 'Choose a Set'] + $sets, Session::get('dbz-filter')['set'], ['id' => 'filter_set', 'class' => 'filter']) }}
	</div>
	<div class="style">
		{{ form::select('style', ['all' => 'Choose a Style'] + $styles, Session::get('dbz-filter')['style'], ['id' => 'filter_style', 'class' => 'filter']) }}
	</div>
	<div class="personality">
		{{ form::select('personality', ['all' => 'Choose a Personality'] + $personalities, Session::get('dbz-filter')['personality'], ['id' => 'filter_personality', 'class' => 'filter']) }}
	</div>
	{{ Form::close() }}
</div>