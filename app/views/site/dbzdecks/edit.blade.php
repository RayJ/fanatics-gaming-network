@extends('site.layouts.fgn')
{{-- Content --}}
@section('content')
<div class="mainPanel {{ $deck->style->title }}">
	<div class="crossbar"></div>
	<div class="crossTitle"><h1>DBZ Deck Builder</h1></div>
	<div id="deckBuilder" class="dbz edit {{ $deck->personality->title }}">
		{{-- var_dump($deck) --}}
		<div class="mastery">
			<h3>Mastery</h3>
			{{ Form::select('mastery', $mastery, '', ['id' => 'mastery']) }}
		</div>
		<div class="personality">
			<h3>Personality</h3>
			<div class="level1">
				{{ Form::select('level1', $level[1], '', ['id' => 'level1']) }}	
				{{ Form::label('level1', 'Level 1') }}
			</div>
			<div class="level2">
				{{ Form::select('level2', $level[2], '', ['id' => 'level2']) }}
				{{ Form::label('level2', 'Level 2') }}
			</div>
			<div class="level3">
				{{ Form::select('level3', $level[3], '', ['id' => 'level3']) }}
				{{ Form::label('level3', 'Level 3') }}
			</div>
			<div class="level4">
				{{ Form::select('level4', $level[4], '', ['id' => 'level4']) }}
				{{ Form::label('level4', 'Level 4') }}
			</div>
		</div>
		<div class="lifeDeck">
			<h3>Life Deck</h3>
			<h5>Allies</h5>
			<div class="allies">
			</div>
			<h5>Dragon Balls</h5>
			<div class="dragonballs">
			</div>
			<h5>Drills</h5>
			<div class="drills">
			</div>
			<h5>Setups</h5>
			<div class="setups">
				<div class="card">
					<div class="image">
						<img src="/assets/img/dbz/cards/set-1/r132.jpg" alt="Black Searching Technique">
					</div>
					<div class="cardTitle">
						<span class="cardTitle">Namekian Dragon Clan</span>	
					</div>
					<div class="amount">
						<input type="text" class="amount" value="2" />
					</div>
					<div class="controls">
						<a href="#" class="increase">+</a>
						<a href="#" class="decrease">-</a>
						<a href="#" class="remove">x</a>
					</div>
				</div>
			</div>
			<h5>Events</h5>
			<div class="events">
			</div>
			<h5>Energy Combats</h5>
			<div class="energy">
			</div>
			<h5>Physical Combats</h5>
			<div class="physical">
			</div>
		</div>
	</div>
</div>
@stop

@section('sidebar')
	@include('site.sidebars.deck-builder')
@overwrite