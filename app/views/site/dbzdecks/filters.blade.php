<div class="filters">
	{{ Form::open(array('id' => 'dbz-deck-filter')) }}
	<div class="toggle">
		{{ Form::checkbox('type[]','1', '', ['id' => 'filter_ally', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_ally', 'Ally') }}
		{{ Form::checkbox('type[]','2', '', ['id' => 'filter_dragonball', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_dragonball', 'Dragon Ball') }}
		{{ Form::checkbox('type[]','3', '', ['id' => 'filter_drill', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_drill', 'Drill') }}
		{{ Form::checkbox('type[]','4', '', ['id' => 'filter_setup', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_setup', 'Setup') }}
		{{ Form::checkbox('type[]','5', '', ['id' => 'filter_event', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_event', 'Event') }}
		{{ Form::checkbox('type[]','6', '', ['id' => 'filter_energy', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_energy', 'Energy Combat') }}
		{{ Form::checkbox('type[]','7', '', ['id' => 'filter_physical', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_physical', 'Physical Combat') }}
		{{ Form::checkbox('type[]','8', '', ['id' => 'filter_personality', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_personality', 'Main Personality') }}
		{{ Form::checkbox('type[]','9', '', ['id' => 'filter_mastery', 'class' => 'filter checkbox']) }}
		{{ Form::label('filter_mastery', 'Mastery') }}
	</div>
	<div class="set">
		{{ form::select('set', ['all' => 'Choose a Set'] + $sets, Session::get('dbz-filter')['set'], ['id' => 'filter_set', 'class' => 'filter']) }}
	</div>
	<div class="style">
		{{ form::select('style', ['all' => 'Choose a Style'] + $styles, Session::get('dbz-filter')['style'], ['id' => 'filter_style', 'class' => 'filter']) }}
	</div>
	<div class="personality">
		{{ form::select('personality', ['all' => 'Choose a Personality'] + $personalities, Session::get('dbz-filter')['personality'], ['id' => 'filter_personality', 'class' => 'filter']) }}
	</div>
	{{ Form::close() }}
</div>