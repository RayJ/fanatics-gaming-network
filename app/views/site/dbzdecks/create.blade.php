@extends('site.layouts.fgn')
{{-- Content --}}
@section('content')
<div class="mainPanel">
	<div class="crossbar"></div>
	<div class="crossTitle"><h1>DBZ Deck Builder</h1></div>
	<div id="deckBuilder" class="dbz games">
		{{ Form::open() }}
		{{ form::select('personality', ['' => 'Choose a Personality'] + $personalities, '', ['id' => 'personality']) }}
		{{ form::select('style', ['' => 'Choose a Style'] + $styles, '', ['id' => 'style']) }}
		{{ Form::label('title', 'Title') }}
		{{ Form::text('title', '', ['id' => 'title']) }}
		{{ Form::label('description', 'Description') }}
		{{ Form::textarea('description', '', ['id' => 'description'])}}
		<div class="row">
			<div class="small-12 medium-6 columns toggle">
				{{ Form::checkbox('public','1', true, ['id' => 'public', 'class' => 'checkbox']) }}
				{{ Form::label('public', 'Make Public') }}
			</div>
			<div class="small-12 medium-6 columns toggle">
				{{ Form::submit('Add Cards', ['class' => 'button submit']) }}
			</div>
		</div>
		
		{{ Form::close() }}
	</div>
</div>
@stop