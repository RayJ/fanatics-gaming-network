<div class="sidebar">
	<div class="crossbar"></div>
	<h3>Deck Builder Controls</h3>
	<div class="controls">
		@include('site.dbzdecks.filters')
	</div>
</div>