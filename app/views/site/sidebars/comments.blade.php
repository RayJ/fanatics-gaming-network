<div class="sidebar">
	<div class="crossbar"></div>
	<h3>Recent Comments</h3>
	<div class="comments textarea">
		@if($comments->count())
		@foreach($comments as $comment)
		<div class="row">
			<div class="small-2 columns">
				<img class="author" src="{{ $comment->author->getProfileImage() }}" alt="">
			</div>
			<div class="small-10 columns">
				<div class="comment">
					<h6><a href="/{{ $comment->post->slug }}">{{ $comment->post->title }}</a></h6>
					<p>{{ String::tidy(Str::limit($comment->content, 150)) }}</p>
				</div>
			</div>
		</div>
		@endforeach
		@else
		<div class="row">None</div>
		@endif
	</div>
</div>