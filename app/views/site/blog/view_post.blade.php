@extends('site.layouts.fgn')

{{-- Web site Title --}}
@section('title')
{{{ String::title($post->title) }}} ::
@parent
@stop

{{-- Update the Meta Title --}}
@section('meta_title')
@parent

@stop

{{-- Update the Meta Description --}}
@section('meta_description')
@parent

@stop

{{-- Update the Meta Keywords --}}
@section('meta_keywords')
@parent

@stop

{{-- Content --}}
@section('content')
<div class="post">
	<img class="postImage" alt="{{ $post->image->title }}" src="{{ asset('assets/img/' . $post->image->category->slug .'/'. $post->image->filename) }}" />
	<div class="crossbar">
		<img class="category" src="{{ asset('assets/img/categories/' . $post->category->slug . '.png') }}" alt="" />
	</div>
	<div class="row postTitle">
		<div class=" small-10 columns">
			<h3>
				{{ String::title($post->title) }}
			</h3>
			<p>
				<span class="glyphicon glyphicon-user"></span> by <span class="muted">{{{ $post->author->username }}}</span>
				| <span class="glyphicon glyphicon-calendar"></span> <!--Sept 16th, 2012-->{{{ $post->date() }}}
				| <span class="glyphicon glyphicon-comment"></span> <a href="#comments">{{$post->comments()->count()}} {{ \Illuminate\Support\Pluralizer::plural('Comment', $post->comments()->count()) }}</a>
			</p>
		</div>
		<div class="postInfo small-2 columns">
		</div>
	</div>
	<div class="row postContent">
		<div class="columns">
			<img class="author" src="{{$post->author->getProfileImage()}}" alt="{{$post->author->username}}">
			<?php 
			$parser = new JBBCode\Parser();
			$parser->addCodeDefinitionSet(new JBBCode\DefaultCodeDefinitionSet());
			$youtube = new YoutubeEmbed();
			$parser->addCodeDefinition($youtube);
			$parser->parse($post->content);
			?>
			<p>{{ $parser->getAsHtml() }}</p>
			<div>
				<span class="badge badge-info">Posted {{{ $post->date() }}}</span>
			</div>
		</div>
	</div>
</div>

<div class="post-comments">
	<a id="comments"></a>
	<div class="row">
		<div class="columns">
			<h4>{{{ $comments->count() }}} Comments</h4>

			@if ($comments->count())
			@foreach ($comments as $comment)
			<div class="row">
				<div class="medium-1 columns">
					<img class="author" src="{{ $comment->author->getProfileImage() }}" alt="">
				</div>
				<div class="medium-11 columns">
					<div class="comment">
							<span class="muted">{{{ $comment->author->username }}}</span>
							&bull;
							<span class="date">	{{{ $comment->date() }}}</span>
							<p class="content">{{{ $comment->content() }}}</p> 
					</div>
				</div>
			</div>
			<hr />
			@endforeach
			@else
			@endif

			@if ( ! Auth::check())
			You need to be logged in to add comments.<br /><br />
			Click <a href="{{{ URL::to('user/login') }}}">here</a> to login into your account.
			@elseif ( ! $canComment )
			You don't have the correct permissions to add comments.
			@else

			@if($errors->has())
			<div class="alert alert-danger alert-block">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
				</ul>
			</div>
			@endif

			<h4>Add a Comment</h4>
			<form  method="post" action="{{{ URL::to($post->slug) }}}">
				<input type="hidden" name="_token" value="{{{ Session::getToken() }}}" />

				<textarea class="medium-12 columns input-block-level" rows="4" name="comment" id="comment">{{{ Request::old('comment') }}}</textarea>

				<div class="form-group">
					<div class="medium-12 columns">
						<input type="submit" class="btn btn-default" id="submit" value="Submit" />
					</div>
				</div>
			</form>
			@endif
		</div>
	</div>
</div>

@stop
