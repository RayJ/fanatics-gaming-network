@extends('site.layouts.fgn')

{{-- Content --}}
@section('content')

<div class="blog">
	@foreach ($posts as $post)
	<div class="row">
		<div class="small-12 columns">
			<div class="row post">
				
				<!-- Post Image -->
				<img class="postImage" alt="{{ $post->image->title }}" src="{{ asset('assets/img/' . $post->image->category->slug .'/'. $post->image->filename) }}" />
				
				<div class="crossbar"></div>
				<!-- Post Title -->
				<div class="postTitle small-8 columns">
					<h4>
						<a href="{{{ $post->url() }}}">
							{{ String::title($post->title) }}
						</a>
					</h4>
					<p>
						<span class="glyphicon glyphicon-user"></span> by <span class="muted">{{{ $post->author->username }}}</span>
						| <span class="glyphicon glyphicon-calendar"></span> <!--Sept 16th, 2012-->{{{ $post->date() }}}
						| <span class="glyphicon glyphicon-comment"></span> <a href="{{{ $post->url() }}}#comments">{{$post->comments()->count()}} {{ \Illuminate\Support\Pluralizer::plural('Comment', $post->comments()->count()) }}</a>
					</p>
				</div>
				<div class="postInfo small-2 small-offset-2 columns">
					<img class="category" src="{{ asset('assets/img/categories/' . $post->category->slug . '.png') }}" />
					<img class="author" src="{{$post->author->getProfileImage()}}" alt="{{$post->author->username}}">
				</div>
				<div class="postContent small-12 columns">
					<p>
						{{ String::tidy(Str::limit($post->content, 200)) }}
					</p>
					<a class="button btn-mini btn-default" href="{{{ $post->url() }}}">Read more</a>
				</div>
			</div>
		</div>
	</div>
@endforeach
</div>

{{ $posts->links() }}

@stop
