@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	{{-- Edit Image Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($image)){{ URL::to('admin/images/' . $image->id . '/edit') }}@endif" autocomplete="off" accept-charset="UTF-8" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

				<!-- Image Title -->
				<div class="form-group {{{ $errors->has('title') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="title">Image Title</label>
						<input class="form-control" type="text" name="title" id="title" value="{{{ Input::old('title', isset($image) ? $image->title : null) }}}" />
						{{{ $errors->first('title', '<span class="help-block">:message</span>') }}}
					</div>
				</div>
				<!-- ./ post title -->

				<!-- Image Filename -->
				<div class="form-group {{{ $errors->has('filename') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="filename">Filename</label>
						<input class="form-control" type="text" name="filename" id="filename" value="{{{ Input::old('filename', isset($image) ? $image->filename : null) }}}" />
						{{{ $errors->first('filename', '<span class="help-block">:message</span>') }}}
					</div>
				</div>
				<!-- ./ filename -->

				<!-- Image Category -->
				<div class="form-group {{{ $errors->has('category_id') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="category_id">Category</label>
                        {{ Form::select('category_id', Category::lists('slug','id'), Input::old('filename', isset($image) ? $image->filename : null), array("class" => "form-control")) }}
						{{{ $errors->first('filename', '<span class="help-block">:message</span>') }}}
					</div>
				</div>
				<!-- ./ filename -->

				<!-- Image File -->
				<div class="form-group {{{ $errors->has('file') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="file">Upload File</label>
						<input class="form-control" type="file" name="file" id="filename" value="{{{ Input::old('file', isset($image) ? $image->file : null) }}}" />
						{{{ $errors->first('file', '<span class="help-block">:message</span>') }}}
					</div>
				</div>
				<!-- ./ file -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Cancel</element>
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-success">Update</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
