<?php

class CategoriesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('categories')->delete();

        $foundryCategory = new Category;
        $foundryCategory->name = 'Fanatics Gaming Network';
        $foundryCategory->slug = 'fgn';
        $foundryCategory->save();

        $dbzCategory = new Category;
        $dbzCategory->name = 'Dragonball Z Collectible Card Game';
        $dbzCategory->slug = 'dbz';
        $dbzCategory->save();

        $swlcgCategory = new Category;
        $swlcgCategory->name = 'Star Wars: The Card Game';
        $swlcgCategory->slug = 'swlcg';
        $swlcgCategory->save();

        $xwingCategory = new Category;
        $xwingCategory->name = 'X-Wing Miniatures Game';
        $xwingCategory->slug = 'xwing';
        $xwingCategory->save();

        $wh40kCategory = new Category;
        $wh40kCategory->name = 'Warhammer 40K: Conquest';
        $wh40kCategory->slug = '40k';
        $wh40kCategory->save();
    }

}
