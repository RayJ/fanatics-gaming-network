<?php

class PostsTableSeeder extends Seeder {

    protected $content = 'Our friends over at <a target="_blank" rel="nofollow" href="http://retrodbzccgcom.fatcow.com/">RetroDBZccg</a> did everyone a favor and used their Dragonball powers to help convince <a target="_blank" rel="nofollow" href="http://paniniamericadbz.wordpress.com/">Panini America</a> to&nbsp;resurrect everyone\'s favorite CCG. And what would a DBZ card game be without DBZ Fanatics and the Fanatics Gaming Network?<br><br>Dez and I are hard at work getting features and content ready for the site. By the end of the week we will have a full-featured online deck builder available with interactive card references. Best of all, it will be mobile friendly and optimized for viewing on Android and iOS platforms!<br>Shortly after that milestone, we will get the integrated forum features set up. That task is a little more time intensive, so Richie and the gang have set up shop over at z<a target="_blank" rel="nofollow" href="http://z-warriors.com/">-warriors.com</a>!<br><br>We also have an online events system coming down the pipeline so everyone can broadcast their tournaments and organized play events. As always, FGN is community driven, and we want to make sure the community has the tools to communicate, collaborate, and be their generally awesome selves.<br><br>Finally, after all the features are sorted out for DBZ, we will expand the network with a focus on the LCGs and Miniature games from Fantasy Flight Games, including Star Wars LCG, Warhammer 40,000: Conquest, and X-Wing Miniatures!<br><br>If you have an interest in writing content for the site, hit us up on <a target="_blank" rel="nofollow" href="https://www.facebook.com/fanaticsgamingnetwork">FaceBook</a>!';

    public function run()
    {
        DB::table('posts')->delete();

        $user_id = User::first()->id;

        DB::table('posts')->insert( array(
            array(
                'user_id'    => $user_id,
                'title'      => 'We\'re back!',
                'slug'       => 'were-back',
                'category_id' => 1,
                'image_id'   => 23,
                'content'    => $this->content,
                'meta_title' => 'FGN - Fanatics Gaming Network is back!',
                'meta_description' => 'DBZ Fanatics is back and ready to help the community for the Dragonball Z Card Game become as epic as it was under Score!',
                'meta_keywords' => 'FGN, Fanatics Gaming Network, DBZ Fanatics, DBZ Card Game, Panini America',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            )
        ));
    }

}
