<?php

class DBZCardSeeder extends Seeder {
	public function run()
    {
    	//DBZ Sets
        DB::table('dbzsets')->delete();
        $sets = array(
            array(
                'title'      => 'Set 1',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );
        DB::table('dbzsets')->insert( $sets );

    	//DBZ Styles
        DB::table('dbzstyles')->delete();
        $styles = array(
            array(
                'title'      => 'Black',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Blue',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Namekian',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Orange',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Red',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Saiyan',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Freestyle',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );
        DB::table('dbzstyles')->insert( $styles );

    	//DBZ Sets
        DB::table('dbzpersonalitytypes')->delete();
        $sets = array(
            array(
                'title'      => 'Main Personality',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Ally',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );
        DB::table('dbzpersonalitytypes')->insert( $sets );

        //DBZ Types
        DB::table('dbztypes')->delete();
        $types = array(
            array(
                'title'      => 'Ally',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Dragon Ball',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Drill',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Setup',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Event',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Energy Combat',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Physical Combat',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Main Personality',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Mastery',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );
        DB::table('dbztypes')->insert( $types );

    	//DBZ Alignments
        DB::table('dbzalignments')->delete();
        $alignments = array(
            array(
                'title'      => 'Hero',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Villain',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );
        DB::table('dbzalignments')->insert( $alignments );

    	//DBZ Level
        DB::table('dbzlevels')->delete();
        $levels = array(
            array(
                'title'      => 'Level 1',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Level 2',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Level 3',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Level 4',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );
        DB::table('dbzlevels')->insert( $levels );

    	//DBZ Rarity
        DB::table('dbzrarity')->delete();
        $rarity = array(
            array(
                'title'      => 'Common',
                'slug'		 => 'C',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Uncommon',
                'slug'		 => 'U',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Rare',
                'slug'		 => 'R',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Ultra Rare',
                'slug'		 => 'UR',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Starter',
                'slug'		 => 'S',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Promo',
                'slug'		 => 'P',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );
        DB::table('dbzrarity')->insert( $rarity );

    	//DBZ Personalities
        DB::table('dbzpersonalities')->delete();
        $personalities = array(
            array(
                'title'      => 'Goku',
                'alignment_id' => 1,
                'personality_type_id' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Gohan',
                'alignment_id' => 1,
                'personality_type_id' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Krillin',
                'alignment_id' => 1,
                'personality_type_id' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Piccolo',
                'alignment_id' => 1,
                'personality_type_id' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Vegeta',
                'alignment_id' => 2,
                'personality_type_id' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Frieza',
                'alignment_id' => 2,
                'personality_type_id' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Trunks',
                'alignment_id' => 1,
                'personality_type_id' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Captain Ginyu',
                'alignment_id' => 2,
                'personality_type_id' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Bulma',
                'alignment_id' => 1,
                'personality_type_id' => 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'ChiChi',
                'alignment_id' => 1,
                'personality_type_id' => 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Chaozu',
                'alignment_id' => 1,
                'personality_type_id' => 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Tenshinhan',
                'alignment_id' => 1,
                'personality_type_id' => 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Yamcha',
                'alignment_id' => 1,
                'personality_type_id' => 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Nappa',
                'alignment_id' => 2,
                'personality_type_id' => 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Guldo',
                'alignment_id' => 2,
                'personality_type_id' => 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Burter',
                'alignment_id' => 2,
                'personality_type_id' => 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Recome',
                'alignment_id' => 2,
                'personality_type_id' => 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => 'Jiece',
                'alignment_id' => 2,
                'personality_type_id' => 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );
        DB::table('dbzpersonalities')->insert( $personalities );

        DB::table('dbzcards')->delete();

        $this->call('DBZStarterSeeder');
        $this->call('DBZCommonSeeder');
        $this->call('DBZUncommonSeeder');
        $this->call('DBZRareSeeder');
        $this->call('DBZUltraRareSeeder');
        $this->call('DBZPromoSeeder');
    }
}