<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbzcardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `DBZ Sets` table
		Schema::create('dbzsets', function($table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('title');
			$table->timestamps();
		});

		// Create the `DBZ Styles` table
		Schema::create('dbzstyles', function($table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('title');
			$table->timestamps();
		});

		// Create the `DBZ Types` table
		Schema::create('dbztypes', function($table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('title');
			$table->timestamps();
		});

		// Create the `DBZ Personality Types` table
		Schema::create('dbzpersonalitytypes', function($table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('title');
			$table->timestamps();
		});

		// Create the `DBZ Alignments` table
		Schema::create('dbzalignments', function($table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('title');
			$table->timestamps();
		});

		// Create the `DBZ Levels` table
		Schema::create('dbzlevels', function($table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('title');
			$table->timestamps();
		});

		// Create the `DBZ Personalities` table
		Schema::create('dbzpersonalities', function($table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('title');
			$table->integer('alignment_id')->unsigned()->nullable()->index();
			$table->integer('personality_type_id')->unsigned()->index();
			$table->timestamps();
			$table->foreign('alignment_id')->references('id')->on('dbzalignments')->onDelete('cascade');
			$table->foreign('personality_type_id')->references('id')->on('dbzpersonalitytypes')->onDelete('cascade');
		});

		// Create the `DBZ Rarity` table
		Schema::create('dbzrarity', function($table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('title');
			$table->String('slug');
			$table->timestamps();
		});

		// Create the `DBZ Cards` table
		Schema::create('dbzcards', function($table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('title');
			$table->text('content')->nullable();
			$table->integer('set_id')->unsigned()->nullable()->index();
			$table->integer('style_id')->unsigned()->nullable()->index();
			$table->integer('type_id')->unsigned()->nullable()->index();
			$table->integer('number')->unsigned()->nullable();
			$table->integer('rarity_id')->unsigned()->nullable()->index();
			$table->integer('alignment_id')->unsigned()->nullable()->index();
			$table->integer('personality_id')->unsigned()->nullable()->index();
			$table->integer('level_id')->unsigned()->nullable()->index();
			$table->integer('pur')->nullable();
			$table->bigInteger('stage0')->unsigned()->nullable();
			$table->bigInteger('stage1')->unsigned()->nullable();
			$table->bigInteger('stage2')->unsigned()->nullable();
			$table->bigInteger('stage3')->unsigned()->nullable();
			$table->bigInteger('stage4')->unsigned()->nullable();
			$table->bigInteger('stage5')->unsigned()->nullable();
			$table->bigInteger('stage6')->unsigned()->nullable();
			$table->bigInteger('stage7')->unsigned()->nullable();
			$table->bigInteger('stage8')->unsigned()->nullable();
			$table->bigInteger('stage9')->unsigned()->nullable();
			$table->bigInteger('stage10')->unsigned()->nullable();
			$table->timestamps();
			$table->foreign('set_id')->references('id')->on('dbzsets')->onDelete('cascade');
			$table->foreign('style_id')->references('id')->on('dbzstyles')->onDelete('cascade');
			$table->foreign('type_id')->references('id')->on('dbztypes')->onDelete('cascade');
			$table->foreign('rarity_id')->references('id')->on('dbzrarity')->onDelete('cascade');
			$table->foreign('alignment_id')->references('id')->on('dbzalignments')->onDelete('cascade');
			$table->foreign('level_id')->references('id')->on('dbzlevels')->onDelete('cascade');
			$table->foreign('personality_id')->references('id')->on('dbzpersonalities')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `DBZ Cards` table
		Schema::drop('dbzcards');
		// Delete the `DBZ Rarity` table
		Schema::drop('dbzrarity');
		// Delete the `DBZ Personalitie` table
		Schema::drop('dbzpersonalities');
		// Delete the `DBZ Levels` table
		Schema::drop('dbzlevels');
		// Delete the `DBZ Alignments` table
		Schema::drop('dbzalignments');
		// Delete the `DBZ Personality Types` table
		Schema::drop('dbzpersonalitytypes');
		// Delete the `DBZ Types` table
		Schema::drop('dbztypes');
		// Delete the `DBZ Styles` table
		Schema::drop('dbzstyles');
		// Delete the `DBZ Sets` table
		Schema::drop('dbzsets');
	}

}
