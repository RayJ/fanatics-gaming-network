<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbzdecksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `DBZ Decks` table
		Schema::create('dbzdecks', function($table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('title');
			$table->text('description');
			$table->integer('style_id')->unsigned()->nullable()->index();
			$table->integer('personality_id')->unsigned()->nullable()->index();
			$table->integer('user_id')->unsigned()->index();
			$table->integer('public')->unsigned()->index();
			$table->timestamps();
			$table->foreign('style_id')->references('id')->on('dbzstyles')->onDelete('cascade');
			$table->foreign('personality_id')->references('id')->on('dbzpersonalities')->onDelete('cascade');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});

		// Creates the 'DBZ Cards<->Decks' (Many-to-Many relation) table
        Schema::create('dbzcards_dbzdecks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('dbzcard_id')->unsigned()->index();
            $table->integer('dbzdeck_id')->unsigned()->index();
            $table->integer('quantity')->unsigned();
            $table->foreign('dbzcard_id')->references('id')->on('dbzcards')->onDelete('cascade');
            $table->foreign('dbzdeck_id')->references('id')->on('dbzdecks')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the 'DBZ Cards<->Decks' (Many-to-Many relation) table
		Schema::drop('dbzcards_dbzdecks');
		// Delete the `DBZ Decks` table
		Schema::drop('dbzdecks');
	}

}
