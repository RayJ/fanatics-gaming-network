<?php

/*
|--------------------------------------------------------------------------
| View Composer Mapping
|--------------------------------------------------------------------------
|
*/

View::composers(array(	
	'BodyClassComposer'     	=> 'site.layouts.master',
	'RecentCommentsComposer'	=> 'site.sidebars.comments',
	'DBZFiltersComposer'		=> 'site.dbzcards.filters',
	'DBZDeckFiltersComposer'	=> 'site.dbzdecks.filters',
));